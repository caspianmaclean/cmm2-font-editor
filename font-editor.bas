Option explicit
Option base 1

'Mode 3,12
'Mode 3,8
CLS

Dim xSize=8
Dim ySize=8

Dim header1$="DefineFont #8"
Dim numChars=95
Dim firstChar=32 'space
Dim lastChar=firstChar+numChars-1
Dim lastArrayChar=255
Dim header64bit$=hex$(numChars,2)+hex$(firstChar,2)+"0808"
'e.g. "01200808"
'01 characters, hex 20 (32) as first character, 8x8
Dim header2$="  "+header64bit$
Dim footer$="End DefineFont"

'This is the output - do not pick a file you want to keep
Dim fileName$="output.fnt"


#include "speccy.inc"

Dim includeFont=0
'Dim includeFont=1
'1 to use the included font, 0 to not use it.


'detect whether a font was given in include file
'on error skip
'font 8
'if mm.errno then
'  Dim includeFont=0
'else
'  Dim includeFont=1
'end if
'font 1
' didn't work

Dim fileName2$=filename$
'This is the start font, if not using the included one.
'Dim fileName2$=""
'empty string for no start font.

'you could set filename2$=filename$ to keep editing a file created by this program

Dim xOffset=5
Dim yOffset=1

Dim xImage=200
Dim yImage=20

Dim xMag=200
Dim yMag=50
Dim mag=4

Dim sBlank$="........    "
Dim fBlank$="00000000    "

'screen representation
Dim s$(ySize)

'file representation will have binary numbers
'storing 1 and zero digits here
Dim f$(ySize)
'it will be converted to hex strings
Dim fh$(ySize)

'all the characters
Dim sAll$(ySize,lastArrayChar)
Dim fAll$(ySize,lastArrayChar)

Dim iChar=firstChar

Function binStringToHex$(bin$)
  Local bin2$="&b"+bin$
  Local v=val(bin2$)
  Local h$=hex$(v,2)
  binStringToHex$=h$
End function

Dim y,x

Sub LoadFromImage
  For x=1 to xSize
    For y=1 to ySize
      Select case pixel(x-1+xImage,y-1+yImage)
        Case RGB(black)
          Mid$(s$(y),x,1)="."
          Mid$(f$(y),x,1)="0"
        Case else
          Mid$(s$(y),x,1)="#"
          Mid$(f$(y),x,1)="1"
      End select
    Next
  Next
'pause 1000
End sub

Sub LoadFromArray i
  For y=1 to ySize
    s$(y)=sAll$(y,i)
    f$(y)=fAll$(y,i)
  Next
End sub

Sub SaveToArray i
  For y=1 to ySize
    sAll$(y,i)=s$(y)
    fAll$(y,i)=f$(y)
  Next
End sub

Sub RotateRight
  For y=1 to ySize
    s$(y)=mid$(s$(y),xSize,1)+left$(s$(y),xSize-1)
    f$(y)=mid$(f$(y),xSize,1)+left$(f$(y),xSize-1)
  Next
End sub

Sub RotateLeft
  For y=1 to ySize
    s$(y)=mid$(s$(y),2,xSize-1)+left$(s$(y),1)
    f$(y)=mid$(f$(y),2,xSize-1)+left$(f$(y),1)
  Next
End sub

Sub RotateUp
  local tops$=s$(1)
  local topf$=f$(1)
  For y=1 to ySize-1
    s$(y)=s$(y+1)
    f$(y)=f$(y+1)
  Next
  s$(ySize)=tops$
  f$(ySize)=topf$
End sub

Sub RotateDown
  local bottoms$=s$(ySize)
  local bottomf$=f$(ySize)
  For y=ySize to 2 step -1
    s$(y)=s$(y-1)
    f$(y)=f$(y-1)
  Next
  s$(1)=bottoms$
  f$(1)=bottomf$
End sub

For iChar=firstChar to lastChar
  For y=1 to ySize
    sAll$(y,iChar)=sBlank$
    fAll$(y,iChar)=fBlank$
  Next
Next

For y=1 to ySize
  s$(y)=sBlank$
  f$(y)=fBlank$
Next

if includeFont or fileName2$ <> "" then
  if includeFont=0 then
    Load font fileName2$
  end if
  Box xImage-2,yImage-2,8+4,8+4,1,RGB(green)
  Font 8
  for iChar=firstChar to lastChar
    Print @(xImage, yImage) chr$(iChar);
    LoadFromImage
    SaveToArray iChar
  next
  Font 1
end if

iChar=firstChar

LoadFromArray iChar

Sub LocateEntry x,y
  LocateCell x+xOffset,y+yOffset
End sub

Sub PrintGrid
  For y=1 to ySize
    For x=1 to xSize
      LocateEntry x,y
      Print mid$(s$(y),x,1);
    Next x
  Next y
  LocateEntry 3,ySize+2
  Print iChar;"     "
End sub

PrintGrid

Print
Print
Print "Controls: ZXOK (zxok) to move, space or 1-7 to set, 0 or C or P to clear"
Print "Capital ZXOK to rotate all character pixels."
Print "S to save and display. Warning, will overwrite previous save"
Print "+ or - to move between character numbers"
Print "File name: ";fileName$
Print "Q to quit"

Sub SaveFont
  'Print "Please wait until finished..."
  Open fileName$ for output as #1
  
  Print #1, header1$
  Print #1, header2$
  SaveToArray iChar
  Print #1, " ";  

  For iChar=firstChar to LastChar

    LoadFromArray iChar

    For y=1 to ySize
      fh$(y) = binStringToHex$(f$(y))
    Next

    Print #1, " ";

    Print #1, fh$(4);
    Print #1, fh$(3);
    Print #1, fh$(2);
    Print #1, fh$(1);

    Print #1, " ";

    Print #1, fh$(8);
    Print #1, fh$(7);
    Print #1, fh$(6);
    Print #1, fh$(5);
    If iChar=lastChar then
      Print #1
    Else If (iChar-firstChar+1) mod 4 = 0 then
      Print #1
      Print #1, " ";  
    End if
  Next iChar
  Print #1, footer$

  Close #1
  'Print "Done"
  Load font fileName$
  Box xImage-2,yImage-2,numChars*8+4,8+4,0,0,RGB(blue)
  Font 8
  Print @(xImage, yImage) " !";chr$(34);"#$%&'";
  Font 1
  'Sprite load fileName$,1
  'Sprite show 1, xImage, yImage,1
  Image resize_fast xImage, yImage, xSize, ySize, xMag, yMag, mag * xSize, mag * ySize

  'Sprite close all
  'Pause 500
End sub

x=1
y=1

Dim c$
Do
  LocateEntry x,y
  Print "C"
  Pause 50
  c$=inkey$
  LocateEntry x,y
  Print mid$(s$(y),x,1);    
  Select case c$
    Case "z"
      x=x-1
    Case "x"
      x=x+1
    Case "o"
      y=y-1
    Case "k"
      y=y+1
    Case "Z"
      RotateLeft
      PrintGrid
    Case "X"
      RotateRight
      PrintGrid
    Case "O"
      RotateUp
      PrintGrid
    Case "K"
      RotateDown
      PrintGrid
    Case "+"
      SaveToArray iChar
      iChar=min(iChar+1,lastChar)
      LoadFromArray iChar
      PrintGrid
    Case "-"
      SaveToArray iChar
      iChar=max(iChar-1,firstChar)
      LoadFromArray iChar
      PrintGrid
    Case "q","Q"
quit: End
    Case "c","C","0","p","P"
      Mid$(s$(y),x,1)="."
      Mid$(f$(y),x,1)="0"
    Case "1" to "7", "#"," "
      Mid$(s$(y),x,1)="#"
      Mid$(f$(y),x,1)="1"
    Case "S","s"
      SaveFont
  End select
  x=min(x,xSize)
  x=max(x,1)
  y=min(y,ySize)
  y=max(y,1)
Loop


Sub LocateCell x,y
  Print @(x*MM.Info(FontWidth),y*MM.Info(FontHeight));
  'I tried without a semicolon and it didn't work right.
End Sub


