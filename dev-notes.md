Documenting code style used by the program:

It uses $ for strings, % would be for integers if needed, defaults to floating point.

Basic keywords using sentence case - the initial command word is capitalised, second word
of command, or parameter words and functions maybe lowercase. Examples
End sub
Option explicit

Multi-word keywords include spaces where it's optional, e.g.:
End sub
End if

Multi-word names without spaces have later words capitalised, e.g. camelCase if the initial word isn't capitalised.
e.g.
Function binStringToHex$(bin$)

Sub names capitalised, e.g.
LoadFromArray i
Sub LoadFromArray i

CLS is uppercase like it's an acronym.

At least for the .bas files, line endings use the CMM2 convention (noted because sometimes git or unix editors change this)
---
  
Document font format? I haven't got complete documentation - I got the bit and byte order by trial and error, not
sure how it would extend to different font sizes.
I've seen the header described though. And there is some documentation linked from the colour maximite 2 manual.

---

Ideas for improvements:

* make sure it works over a terminal connection, to the extent that it can
* * maybe allow using terminal keyboard, but use VGA for graphics.
* allow different font size
* allow choosing the file to edit, load, or save. For loading, could make a file picker similar in style to the CMM2's file picker
* use a custom font to make the editing grid look better
* make the pixel representation's aspect ratio square, not rectangular
* use graphics to make the editing grid look better
* improve layout to look better. Idea:
* * square editing grid top left
* * Grid of up to 32x8 characters at the bottom (normally fewer)
* * * If characters to large to all fit full size, scale them down
* * Top right can have instructions and menu
* * Change layout for more instructions
* * change layout for selecting file to load
* allow using arrow keys
* improve other key choices
* display a program version number
* check for required firmware version - if we know what it needs.


